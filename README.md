# CookQuick API

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sylvanld_cookquick-api&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=sylvanld_cookquick-api)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sylvanld_cookquick-api&metric=coverage)](https://sonarcloud.io/summary/new_code?id=sylvanld_cookquick-api)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=sylvanld_cookquick-api&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=sylvanld_cookquick-api)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=sylvanld_cookquick-api&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=sylvanld_cookquick-api)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=sylvanld_cookquick-api&metric=bugs)](https://sonarcloud.io/summary/new_code?id=sylvanld_cookquick-api)

API that enables storage / search of recipes, building shopping list, and shopping together

