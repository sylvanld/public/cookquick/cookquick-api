from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

Base = declarative_base()


class Database:
    def __init__(self, database_url: str):
        self.engine = create_engine(database_url)
        self.session = scoped_session(session_factory=sessionmaker(bind=self.engine))

    def create_all(self):
        Base.metadata.create_all(self.engine)

    def drop_all(self):
        Base.metadata.drop_all(self.engine)
