"""
Entrypoint used to run REST API.
Can be used as follow :

    uvicorn --factory cookquick.server:CookQuickAPI
"""
from fastapi import FastAPI

from cookquick import metadata


class CookQuickAPI(FastAPI):
    """
    API object where middlewares and endpoints are registered.
    """

    def __init__(self):
        super().__init__(
            title=metadata.TITLE,
            description=metadata.DESCRIPTION,
            version=metadata.VERSION,
            docs_url="/",
        )
