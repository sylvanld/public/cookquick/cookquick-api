"""
Describe how to install python application as a package.
"""
import setuptools

from cookquick import metadata


def get_requirements(filename):
    """
    Load requirements from given file.
    """
    requirements = []

    with open(filename, encoding="utf-8") as requirements_file:
        for row in requirements_file.read().split("\n"):
            if not row.startswith("#"):
                requirements.append(row.strip())

    return requirements


setuptools.setup(
    name=metadata.title.replace(" ", "-").lower(),
    description=metadata.description,
    version=metadata.description,
    install_requires=get_requirements("requirements/prod.txt"),
)
